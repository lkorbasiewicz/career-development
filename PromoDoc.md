# Promotion for **[Team Member Name - link LinkedIn profile]** from **[Current Position]** to **[Proposed Position]**

_Remember:_

- _Promotions are based on performance, not potential as outlined in the [handbook](https://about.gitlab.com/handbook/people-group/promotions-transfers/#for-managers-requesting-a-promotion-or-compensation-change)._
- _Values should be at the forefront of the proposal and therefore are at the top of the document._

_Per the [handbook](https://about.gitlab.com/handbook/people-group/promotions-transfers/#creating-a-promotion-or-compensation-change-document), please be sure that the promotion document has &quot;comment&quot; access enabled to &#39;**Anyone at GitLab who has the link!&#39;** to ensure the review and approval process is not delayed._

_Please delete the instructions associated with each section of the promotion document below before submitting the promotions._

---

## Manager Summary

This document is a record of achievements and testimonials demonstrating how **[Team Member Name]** has been performing at the **[Proposed Position]** level.

**[Summary of Achievements]**

---

## Values Alignment

_Values are at the core of everything we do at GitLab. It is essential to tie team member achievements and initiatives back to the values they support._

### [Collaboration](https://about.gitlab.com/handbook/values/#collaboration)

**[Evidence Supporting Values Alignment]**

### [Results](https://about.gitlab.com/handbook/values/#results)

**[Evidence Supporting Values Alignment]**

### [Efficiency](https://about.gitlab.com/handbook/values/#efficiency)

**[Evidence Supporting Values Alignment]**

### [Diversity, Inclusion, and Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion)

**[Evidence Supporting Values Alignment]**

### [Iteration](https://about.gitlab.com/handbook/values/#iteration)

**[Evidence Supporting Values Alignment]**

### [Transparency](https://about.gitlab.com/handbook/values/#transparency)

**[Evidence Supporting Values Alignment]**

---

## Leadership Skills

**_Leadership skills are required to include for promotions for people managers, but not required for individual contributor positions._**

**Please reference the [leadership page](https://about.gitlab.com/handbook/leadership/) in the handbook for guidance pertaining to expectations at the various levels of leadership at GitLab.**

- [**Management group**](https://about.gitlab.com/handbook/leadership/#management-group)
- [**Director group**](https://about.gitlab.com/handbook/leadership/#director-group)
- [**S-group**](https://about.gitlab.com/handbook/leadership/#s-group)
- [**E-group**](https://about.gitlab.com/handbook/leadership/#e-group)

---

---

_Link issues/MRs/achievements that highlight contributions, projects, ideas, initiatives, etc. that showcase the team member&#39;s expertise in their area. This will look different depending on the role of each team member. For a guideline, it can be helpful to refer to the responsibilities outlined in the team member&#39;s job family, both at their current level and proposed level._

---

## Feedback (optional)

_This optional section of the promotion template is meant to provide a space to include any positive feedback the team member has received (I.E. from our #thanks Slack channel, discretionary bonuses, emails highlighting something the team member did well, 360 feedback, etc.)._

_Feedback must be:_

- _From GitLab team members and can come from both peers and leadership on the team members team or in other teams, **or**_
- _Customers/partners for field facing teams_

**Important Notes:**

- **Please do not ask other team members to provide feedback for the purpose of this promotion document as this could be a violation of personal privacy. All feedback provided below should be feedback provided organically prior to the creation of this document.**
- **All feedback included should be anonymized**
