# Values Based Tracker - Łukasz Korbasiewicz

__Purpose: Document and organize contributions on the Support team as they relate to GitLab CREDIT values.__

### Accomplishments to Classify:
_add your accomplishments here if you don’t know where they should be classified_

## Values

### Collaboration
_add your accomplishments here_

### Results
_add your accomplishments here_

### Efficiency
_add your accomplishments here_

### Diversity, Inclusion, & Belonging
_add your accomplishments here_

### Iteration 
_add your accomplishments here_

### Transparency 
_add your accomplishments here_



## Feedback

### Positive Feedback
_add positive feedback you have received here - maybe it’s a screenshot of a slack message or issue comment_
