# Individual Growth Plan

| Team Member | Manager | Date created | Date last updated |
| ------ | ------ | ------ | ------ |
|  your_name | your_managers_name  | date_created  | last_update_date  |

---

## Purpose

_Your Individual Growth Plan (IGP) outlines your professional goals and the steps you will take to get there. You will work with your manager to identify opportunities to develop your skills or knowledge. The areas for development you identify should be important for GitLab and your career._

## Section 1: Discover your Inspiration

_Use the questions below to think about what inspires you in your current role._


| Discover your inspiration |  |
| ------ | ------ |
| __When do you feel most engaged by your current role?__ |  |
| What work are you doing when you feel that you belong? |  |
| What areas of your work excite you? |  |
| What areas of work feel like energy vampires? |  |
| When do you feel you are learning? |  |
| When do you achieve results that you are proud of? |  |

## Section 2: Select a Focus

_Reflect upon your future. What new role or focus will also supply you with inspiration?_

| Select a focus |  |
| ------ | ------ |
| __What role, team or focus will preserve your inspiration and provide you with more?__ |  |
| Do you want to stay in the team you are in? |  |
| Would you like to manage a team? |  |
| Would you like to manage a department? |  |
| Would you like to stay as an individual contributor and develop a specific area of expertise? |  |
| __Briefly describe how this new role or focus will provide you with a source of inspiration?__ |  |

## Section 3: How do we get there

| **Development<br>Objective**<br>*Specific<br>Skills/Behaviors* | **Baseline**<br>*What are your<br>current metrics?* | **Impact**<br>*Why does this matter<br>to you or the team?* | **Action Steps**<br>*Measurable &<br>Achievable* | **Manager/Peer<br>Support**<br>*Education,<br>Exposure,<br>Experience* | **Hurdles you Anticipate** |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 1. |  |  | a. |  |  |
| 2. |  |  | b. |  |  |
| 3. |  |  | c. |  |  |


| **Reflecting on your<br>development**<br>*Outline development<br>objective* | **Feedback**<br>*What feedback<br>did you receive?* | **Situational Awareness**<br>*Was there any response that<br>you want to explore further?<br>Who can you talk to?* | **Focus on Impact**<br>*What parts of the<br>feedback are in your<br>sphere of influence?* | **Problem Solve**<br>*What can you do<br>differently next time?* |
| ------ | ------ | ------ | ------ | ------ |
| 1. |  |  |  |  |
| 2. |  |  |  |  |
| 3. |  |  |  |  |

